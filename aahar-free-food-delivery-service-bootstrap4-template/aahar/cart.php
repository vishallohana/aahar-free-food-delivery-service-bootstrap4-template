<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cart || Aahar Food Delivery Html5 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/icon.png">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/plugins.css">
    <link rel="stylesheet" href="style.css">

    <!-- Cusom css -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Modernizer js -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->

<!-- <div class="fakeloader"></div> -->

<!-- Main wrapper -->
<div class="wrapper" id="wrapper">
    <!-- Start Header Area -->

    <?php


    require 'header.php';

    $connect = mysqli_connect("localhost", "root", "", "aahar");

    $i = 0;
    if (isset($_GET['pid'])) {
        $did = $_GET['pid'];
        unset($_SESSION['productcart'][$did]);
        unset($_SESSION['qtycart'][$did]);

    }

    ?>

    <!-- End Header Area -->
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--18">
        <div class="ht__bradcaump__wrap d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="bradcaump__inner text-center">
                            <h2 class="bradcaump-title">cart page</h2>
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="index.html">Home</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                <span class="breadcrumb-item active">cart page</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->


    <!-- cart-main-area start -->
    <?php

    if (empty($_SESSION['productcart'])) {
        echo "<br><h4 align='center'>Your cart is empty, Kindly add products <a href='menu-list.php'>Continue Shopping</a></h4>";
    }


    if (isset($_SESSION['productcart']) && !empty($_SESSION['productcart'])) {


        echo "      
        <div class='cart-main-area section-padding--lg bg--white'>
            <div class='container'>
                <div class='row'>
                    <div class='col-md-12 col-sm-12 ol-lg-12'>
                        <form action='#'>               
                            <div class='table-content table-responsive'>
   
  <table>
                                    <thead>
                                        
                                            <tr class='title-top'>
                                            <th class='product-thumbnail'>Image</th>
                                            <th class='product-name'>Product</th>
                                            <th class='product-price'>Price (INR)</th>
                                            <th class='product-quantity'>Quantity</th>
                                            <th class='product-subtotal'>Total (INR)</th>
                                            <th class='product-remove'>Remove</th>
                                        </tr>
                                    </thead>";


        foreach ($_SESSION['productcart'] as $key => $value) {

            $i++;
            $qty = $_SESSION['qtycart'][$key];

            $proq = mysqli_query($connect, "SELECT * FROM meal WHERE meal_id='{$value}'") or die(mysqli_error($connect));


            $prorow = mysqli_fetch_array($proq);


            $qty = $_SESSION['qtycart'][$key];

            $Subtotal = $prorow['meal_price'] * $qty;

            $grandtotal[] = $Subtotal;
            $sumtotal = array_sum($grandtotal);
            $i++;
            echo "                                <tr>
                                            <td class='product-thumbnail'><a href='#'><img src='upload/{$prorow['meal_image']}' alt='product img' /></a></td>
                                            <td class='product-name'><strong>{$prorow['meal_name']}</strong></td>
                                            <td class='product-price'><span class='amount'>{$prorow['meal_price']}</span></td>
                                            <td class='product-quantity'><label for='qty'>{$qty}</label></td>
                                            <td class='product-subtotal'>{$Subtotal}</td>
                                            <td class='product-remove'><a href='?pid=$key'>Delete</a></td>
                                        </tr>
                                  

";


        }
        echo "    
                                    </tbody>
                                </table>
</div>
</div>
</div></div>
</div>";
        echo "
                            </div>
                        </form> 
                        <div class='cartbox__btn'>
                            <ul class='cart__btn__list d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between'>
                                <li><a href='#'>Coupon Code</a></li>
                                <li><a href='#'>Apply Code</a></li>
                                <li><a href='menu-list.php'>Update Cart</a></li>
";
        ?>
        <?php

        if (isset($_SESSION['user_id'])) {

            ?>
            <li><a href='checkout.php'>Check Out</a></li>
            <?php
        } else {
            ?>
            <li><a href='userlogin.php'>Check Out</a></li>
            <?php
        }
        ?>
        <?php
        echo "

                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-6 offset-lg-6'>
                        <div class='cartbox__total__area'>
                            <div class='cartbox-total d-flex justify-content-between'>
                                <ul class='cart__total__list'>
                                    <li>Cart total</li>
                                    <li>Sub Total</li>
                                </ul>
                                <ul class='cart__total__tk'>
                                    <li>Rs. {$sumtotal}</li>
                                    <li>0</li>
                                </ul>
                            </div>
                            <div class='cart__total__amount'>
                                <span>Grand Total</span>
                                <span>Rs. {$sumtotal}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>";
    }


    ?>
    <br>

    <!-- cart-main-area end -->
    <!-- Start Footer Area -->

    <?php
    include 'footer.php';
    ?>
    <!-- End Footer Area -->
    <!-- Login Form -->
    <div class="accountbox-wrapper">
        <div class="accountbox text-left">
            <ul class="nav accountbox__filters" id="myTab" role="tablist">
                <li>
                    <a class="active" id="log-tab" data-toggle="tab" href="#log" role="tab" aria-controls="log"
                       aria-selected="true">Login</a>
                </li>
                <li>
                    <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                       aria-selected="false">Register</a>
                </li>
            </ul>
            <div class="accountbox__inner tab-content" id="myTabContent">
                <div class="accountbox__login tab-pane fade show active" id="log" role="tabpanel"
                     aria-labelledby="log-tab">
                    <form action="#">
                        <div class="single-input">
                            <input class="cr-round--lg" type="text" placeholder="User name or email">
                        </div>
                        <div class="single-input">
                            <input class="cr-round--lg" type="password" placeholder="Password">
                        </div>
                        <div class="single-input">
                            <button type="submit" class="food__btn"><span>Go</span></button>
                        </div>
                        <div class="accountbox-login__others">
                            <h6>Or login with</h6>
                            <div class="social-icons">
                                <ul>
                                    <li class="facebook"><a href="https://www.facebook.com/"><i
                                                    class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="pinterest"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="accountbox__register tab-pane fade" id="profile" role="tabpanel"
                     aria-labelledby="profile-tab">
                    <form action="#">
                        <div class="single-input">
                            <input class="cr-round--lg" type="text" placeholder="User name">
                        </div>
                        <div class="single-input">
                            <input class="cr-round--lg" type="email" placeholder="Email address">
                        </div>
                        <div class="single-input">
                            <input class="cr-round--lg" type="password" placeholder="Password">
                        </div>
                        <div class="single-input">
                            <input class="cr-round--lg" type="password" placeholder="Confirm password">
                        </div>
                        <div class="single-input">
                            <button type="submit" class="food__btn"><span>Sign Up</span></button>
                        </div>
                    </form>
                </div>
                <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
            </div>
        </div>
    </div><!-- //Login Form -->
    <!-- Cartbox -->
    <div class="cartbox-wrap">
        <div class="cartbox text-right">
            <button class="cartbox-close"><i class="zmdi zmdi-close"></i></button>
            <div class="cartbox__inner text-left">
                <div class="cartbox__items">
                    <!-- Cartbox Single Item -->
                    <div class="cartbox__item">
                        <div class="cartbox__item__thumb">
                            <a href="product-details.html">
                                <img src="images/1.jpg" alt="small thumbnail">
                            </a>
                        </div>
                        <div class="cartbox__item__content">
                            <h5><a href="product-details.html" class="product-name">Vanila Muffin</a></h5>
                            <p>Qty: <span>01</span></p>
                            <span class="price">$15</span>
                        </div>
                        <button class="cartbox__item__remove">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div><!-- //Cartbox Single Item -->
                    <!-- Cartbox Single Item -->
                    <div class="cartbox__item">
                        <div class="cartbox__item__thumb">
                            <a href="product-details.html">
                                <img src="images/blog/sm-img/2.jpg" alt="small thumbnail">
                            </a>
                        </div>
                        <div class="cartbox__item__content">
                            <h5><a href="product-details.html" class="product-name">Wheat Bread</a></h5>
                            <p>Qty: <span>01</span></p>
                            <span class="price">$25</span>
                        </div>
                        <button class="cartbox__item__remove">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div><!-- //Cartbox Single Item -->
                    <!-- Cartbox Single Item -->
                    <div class="cartbox__item">
                        <div class="cartbox__item__thumb">
                            <a href="product-details.html">
                                <img src="images/blog/sm-img/3.jpg" alt="small thumbnail">
                            </a>
                        </div>
                        <div class="cartbox__item__content">
                            <h5><a href="product-details.html" class="product-name">Mixed Fruits Pie</a></h5>
                            <p>Qty: <span>01</span></p>
                            <span class="price">$30</span>
                        </div>
                        <button class="cartbox__item__remove">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div><!-- //Cartbox Single Item -->
                </div>
                <div class="cartbox__total">
                    <ul>
                        <li><span class="cartbox__total__title">Subtotal</span><span class="price">$70</span></li>
                        <li class="shipping-charge"><span class="cartbox__total__title">Shipping Charge</span><span
                                    class="price">$05</span></li>
                        <li class="grandtotal">Total<span class="price">$75</span></li>
                    </ul>
                </div>
                <div class="cartbox__buttons">
                    <a class="food__btn" href="cart.html"><span>View cart</span></a>
                    <a class="food__btn" href="checkout.html"><span>Checkout</span></a>
                </div>
            </div>
        </div>
    </div><!-- //Cartbox -->
</div><!-- //Main wrapper -->

<!-- JS Files -->
<script src="js/vendor/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/active.js"></script>
</body>
</html>
