<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Menu Details || Aahar Food Delivery Html5 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/icon.png">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/plugins.css">
    <link rel="stylesheet" href="style.css">

    <!-- Cusom css -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Modernizer js -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->

<!-- <div class="fakeloader"></div> -->

<!-- Main wrapper -->
<div class="wrapper" id="wrapper">
    <!-- Start Header Area -->
    <?php
    require 'header.php';
    ?>


    <!-- End Header Area -->
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--18">
        <div class="ht__bradcaump__wrap d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="bradcaump__inner text-center">
                            <h2 class="bradcaump-title">Menu Details</h2>
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="index.html">Home</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                <span class="breadcrumb-item active">Menu Details</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->

    <?php

    $connect = mysqli_connect("localhost", "root", "", "aahar");

    $pid = $_GET['pid'];

    $mealq = mysqli_query($connect, "select * from meal where meal_id = '{$pid}'");

    $count = mysqli_fetch_array($mealq);


    $mealdisplay = mysqli_query($connect, "SELECT
    `meal`.`meal_id`
    , `meal`.`meal_name`
    , `meal`.`meal_description`
    , `meal`.`meal_price`
    , `meal`.`meal_image`
    , `meal`.`cuisine_id`
    , `cuisine`.`cuisine_name`
FROM
    `aahar`.`meal`
    INNER JOIN `aahar`.`cuisine` 
        ON (`meal`.`cuisine_id` = `cuisine`.`cuisine_id`);
                
      ");

    $mealrow = mysqli_fetch_array($mealdisplay);

    if ($count < 0) {
        header("location:menu-list.php");
    } else {

        echo "



        <!-- Start Blog List View Area -->
        <section class='blog__list__view section-padding--lg menudetails-right-sidebar bg--white'>
            <div class='container'>
                <div class='row'>
                    <div class='col-lg-12 col-md-12 col-sm-12'>
                        <div class='food__menu__container'>
                            <div class='food__menu__inner d-flex flex-wrap flex-md-nowrap flex-lg-nowrap'>
                                <div class='food__menu__thumb'>
                                    <img style='width:450px;height:300px' src='upload/{$count['meal_image']}' alt='images'>
                                </div>
                                <div class='food__menu__details'>
                                    <div class='food__menu__content'>
                                        <h2>{$count['meal_name']}</h2>
                                        <ul class='food__dtl__prize d-flex'>
                                            
                                            <li>Rs. {$count['meal_price']}</li>
                                        </ul>

                                        <p>&nbsp;</p>
                                        <div class='product-action-wrap'>
                                            <div class='prodict-statas'><span>Cuisine : {$mealrow['cuisine_name']} </span></div>

                                            <div class='product-quantity'>
                                                <form id='myform' method='get' action='addtocart.php'>
                                                    <div class='product-quantity'>
                                                        <div>
                                                        <input type='hidden' name='pid' value='{$pid} '>
                                                            
                                                            <input type='number' class='form-control my-2' name='qty' placeholder='Quantity' value='1' min='1' max='11'>

                                                            <div class='add__to__cart__btn my-4'>
                                                                <input type='submit' class='btn btn-danger form-control' value='Add To Cart'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Start Product Descrive Area -->
                            <div class='menu__descrive__area'>
                                <div class='menu__nav nav nav-tabs' role='tablist'>
                                    <a class='active' id='nav-all-tab' data-toggle='tab' href='#nav-all' role='tab'>Description</a>
                                    
                                </div>
                                <!-- Start Tab Content -->
                                <div class='menu__tab__content tab-content' id='nav-tabContent'>
                                    <!-- Start Single Content -->
                                    <div class='single__dec__content fade show active' id='nav-all' role='tabpanel'>
                                        <p>
                                            {$mealrow['meal_description']}
                                        </p>
                                    </div>
                                    
";


    }

    ?>


    <!-- End Single Content -->
</div>
<!-- End Tab Content -->
</div>
<!-- End Product Descrive Area -->
</div>

<!-- End Recent Post -->
<!-- Start Category Area -->

<!-- End Category Area -->
<!-- Start Sidebar Contact -->

<!-- End Sidebar Contact -->
<!-- Start Sidebar Newsletter -->

<!-- End Sidebar Newsletter -->
<!-- Start Sidebar Instagram -->

<!-- End Sidebar Instagram -->
</div>
</div>
</div>
</div>
</section>
<!-- End Blog List View Area -->
<!-- Start Footer Area -->
<?php
require 'footer.php';

?>
<!-- End Footer Area -->
<!-- Login Form -->
<div class="accountbox-wrapper">
    <div class="accountbox text-left">
        <ul class="nav accountbox__filters" id="myTab" role="tablist">
            <li>
                <a class="active" id="log-tab" data-toggle="tab" href="#log" role="tab" aria-controls="log"
                   aria-selected="true">Login</a>
            </li>
            <li>
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                   aria-selected="false">Register</a>
            </li>
        </ul>
        <div class="accountbox__inner tab-content" id="myTabContent">
            <div class="accountbox__login tab-pane fade show active" id="log" role="tabpanel" aria-labelledby="log-tab">
                <form action="#">
                    <div class="single-input">
                        <input class="cr-round--lg" type="text" placeholder="User name or email">
                    </div>
                    <div class="single-input">
                        <input class="cr-round--lg" type="password" placeholder="Password">
                    </div>
                    <div class="single-input">
                        <button type="submit" class="food__btn"><span>Go</span></button>
                    </div>
                    <div class="accountbox-login__others">
                        <h6>Or login with</h6>
                        <div class="social-icons">
                            <ul>
                                <li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="pinterest"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="accountbox__register tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <form action="#">
                    <div class="single-input">
                        <input class="cr-round--lg" type="text" placeholder="User name">
                    </div>
                    <div class="single-input">
                        <input class="cr-round--lg" type="email" placeholder="Email address">
                    </div>
                    <div class="single-input">
                        <input class="cr-round--lg" type="password" placeholder="Password">
                    </div>
                    <div class="single-input">
                        <input class="cr-round--lg" type="password" placeholder="Confirm password">
                    </div>
                    <div class="single-input">
                        <button type="submit" class="food__btn"><span>Sign Up</span></button>
                    </div>
                </form>
            </div>
            <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
        </div>
    </div>
</div><!-- //Login Form -->
<!-- Cartbox -->
<div class="cartbox-wrap">
    <div class="cartbox text-right">
        <button class="cartbox-close"><i class="zmdi zmdi-close"></i></button>
        <div class="cartbox__inner text-left">
            <div class="cartbox__items">
                <!-- Cartbox Single Item -->
                <div class="cartbox__item">
                    <div class="cartbox__item__thumb">
                        <a href="product-details.html">
                            <img src="images/blog/sm-img/1.jpg" alt="small thumbnail">
                        </a>
                    </div>
                    <div class="cartbox__item__content">
                        <h5><a href="product-details.html" class="product-name">Vanila Muffin</a></h5>
                        <p>Qty: <span>01</span></p>
                        <span class="price">$15</span>
                    </div>
                    <button class="cartbox__item__remove">
                        <i class="fa fa-trash"></i>
                    </button>
                </div><!-- //Cartbox Single Item -->
                <!-- Cartbox Single Item -->
                <div class="cartbox__item">
                    <div class="cartbox__item__thumb">
                        <a href="product-details.html">
                            <img src="images/blog/sm-img/2.jpg" alt="small thumbnail">
                        </a>
                    </div>
                    <div class="cartbox__item__content">
                        <h5><a href="product-details.html" class="product-name">Wheat Bread</a></h5>
                        <p>Qty: <span>01</span></p>
                        <span class="price">$25</span>
                    </div>
                    <button class="cartbox__item__remove">
                        <i class="fa fa-trash"></i>
                    </button>
                </div><!-- //Cartbox Single Item -->
                <!-- Cartbox Single Item -->
                <div class="cartbox__item">
                    <div class="cartbox__item__thumb">
                        <a href="product-details.html">
                            <img src="images/blog/sm-img/3.jpg" alt="small thumbnail">
                        </a>
                    </div>
                    <div class="cartbox__item__content">
                        <h5><a href="product-details.html" class="product-name">Mixed Fruits Pie</a></h5>
                        <p>Qty: <span>01</span></p>
                        <span class="price">$30</span>
                    </div>
                    <button class="cartbox__item__remove">
                        <i class="fa fa-trash"></i>
                    </button>
                </div><!-- //Cartbox Single Item -->
            </div>
            <div class="cartbox__total">
                <ul>
                    <li><span class="cartbox__total__title">Subtotal</span><span class="price">$70</span></li>
                    <li class="shipping-charge"><span class="cartbox__total__title">Shipping Charge</span><span
                                class="price">$05</span></li>
                    <li class="grandtotal">Total<span class="price">$75</span></li>
                </ul>
            </div>
            <div class="cartbox__buttons">
                <a class="food__btn" href="cart.html"><span>View cart</span></a>
                <a class="food__btn" href="checkout.html"><span>Checkout</span></a>
            </div>
        </div>
    </div>
</div><!-- //Cartbox -->
</div><!-- //Main wrapper -->

<!-- JS Files -->
<script src="js/vendor/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/active.js"></script>
</body>
</html>
