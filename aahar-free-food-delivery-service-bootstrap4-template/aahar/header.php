<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<?php
session_start();

?>

<!-- Main wrapper -->
<div class="wrapper" id="wrapper">

    <!-- Start Header Area -->
    <header class="htc__header bg--white">
        <!-- Start Mainmenu Area -->
        <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-4 col-md-6 order-1 order-lg-1">
                        <div class="logo">
                            <a href="index.php">
                                <img src="images/logo/foody.png" alt="logo images">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-sm-4 col-md-2 order-3 order-lg-2">
                        <div class="main__menu__wrap">
                            <nav class="main__menu__nav d-none d-lg-block">
                                <ul class="mainmenu">
                                    <li class="drop"><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About</a></li>
                                    <li><a href="menu-list.php">Meals</a></li>
                                    <li class="drop"><a href="cart.php">Cart</a>
                                        <!--                                            <ul class="dropdown__menu">
                                                                                         <li><a href="menu-list.php">Meals</a></li>
                                                                                        <li><a href="menu-details.php">Meal Details</a></li>
                                                                                    </ul>-->
                                    </li>
                                    <li><a href="gallery.php">Gallery</a></li>

                                    <li><a href="contact.php">Contact</a></li>
                                </ul>
                            </nav>

                        </div>
                    </div>


                    <div class="col-lg-1 col-sm-4 col-md-4 order-2 order-lg-3">
                        <div class="header__right d-flex justify-content-end">
                            <div class="log__in">


                                <?php

                                $connect = mysqli_connect("localhost", "root", "", "aahar");

                                //echo $_SESSION['user'];
                                if (isset($_SESSION['user'])) {

                                    $admin = mysqli_query($connect, "SELECT * FROM admin WHERE admin_email == {$_SESSION['user']}");
//                                    $adminName = mysqli_fetch_array($admin);

//                                    $name = $_SESSION['user'];
//                                    echo "Hello " . $name;

                                    echo "<a href='logout.php'>Log Out</a>";

                                } else {
                                    echo "<a href='userlogin.php'>Log In</a>";

                                }
                                ?>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- Mobile Menu -->
                <div class="mobile-menu d-block d-lg-none"></div>
                <!-- Mobile Menu -->
            </div>
        </div>
        <!-- End Mainmenu Area -->
    </header>
    <!-- End Header Area -->

</div>

</body>
</html>



