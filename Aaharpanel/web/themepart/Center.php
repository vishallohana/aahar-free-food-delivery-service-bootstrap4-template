<?php

require './Connect.php';

$csnq = mysqli_query($Connect,"SELECT * FROM `cuisine` ")or die(mysqli_error($Connect));

$csnrow = mysqli_num_rows($csnq);

//echo $csbrow;

$mealq = mysqli_query($Connect,"SELECT * FROM `meal` ")or die(mysqli_error($Connect));
$mealrow = mysqli_num_rows($mealq);



$empq = mysqli_query($Connect,"SELECT * FROM `employee` ")or die(mysqli_error($Connect));
$emprow = mysqli_num_rows($empq);

$cusq = mysqli_query($Connect,"SELECT * FROM `customer` ")or die(mysqli_error($Connect));
$cusrow = mysqli_num_rows($cusq);


echo "<div class='container-fluid'>
                <div class='row'>
                    <!-- Stats -->
                    <div class='outer-w3-agile col-xl'>
                        <div class='stat-grid p-3 d-flex align-items-center justify-content-between bg-primary'>
                            <div class='s-l'>
                                <h5><a href='Cuisineshoww.php'>Cuisine</a></h5>
                                <p class='paragraph-agileits-w3layouts text-white'>Cuisines you are dealing with</p>
                            </div>
                            <div class='s-r'>
                                <h6>{$csnrow}
                                    <i class='far fa-edit' ></i>
                                </h6>
                            </div>
                        </div>
                        <div class='stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-success'>
                            <div class='s-l'>
                                <h5><a href='Mealshow.php'>Meal</a></h5>
                                <p class='paragraph-agileits-w3layouts'>Meals left for sale</p>
                            </div>
                            <div class='s-r'>
                                <h6>{$mealrow}
                                    <i class='far fa-smile'></i>
                                </h6>
                            </div>
                        </div>
                        <div class='stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-danger'>
                            <div class='s-l'>
                                <h5><a href='Employeeshow.php'>Employee</a></h5>
                                <p class='paragraph-agileits-w3layouts'>Connected Employees with Aahar family</p>
                            </div>
                            <div class='s-r'>
                                <h6>{$emprow}
                                    <i class='fas fa-tasks'></i>
                                </h6>
                            </div>
                        </div>
                        <div class='stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-warning'>
                            <div class='s-l'>
                                <h5><a href='Customer.php'>Customer</a></h5>
                                <p class='paragraph-agileits-w3layouts'>Connected Customers with Aahar family</p>
                            </div>
                            <div class='s-r'>
                                <h6>{$cusrow}
                                    <i class='fas fa-users'></i>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";
?>