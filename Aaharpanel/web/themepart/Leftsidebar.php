<nav id="sidebar">
            <div class="sidebar-header">
                <h1>
                    <a href="index.php">Tiffin Service</a>
                </h1>
                <span>M</span>
            </div>
            <div class="profile-bg"></div>
            <ul class="list-unstyled components">
                <li class="active">
                    <a href="index.php">
                        <i class="fas fa-th-large"></i>
                        Dashboard
                    </a>
                </li>
                <!-- <li>
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-laptop"></i>
                        Components
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="cards.html">Cards</a>
                        </li>
                        <li>
                            <a href="carousels.html">Carousels</a>
                        </li>
                        <li>
                            <a href="forms.html">Forms</a>
                        </li>
                        <li>
                            <a href="modals.html">Modals</a>
                        </li>
                        <li>
                            <a href="tables.html">Tables</a>
                        </li>
                    </ul>
                </li> -->
                <li>
                    <a href="Cuisinee.php">
                        <i class="fas fa-chart-pie"></i>
                        Cuisine
                    </a>
                </li>
                <li>
                    <a href="Meall.php">
                        <i class="fas fa-chart-pie"></i>
                        Meal
                    </a>
                </li>
                <li>
                    <a href="Employee.php">
                        <i class="fas fa-th"></i>
                        Employee
                    </a>
                </li>
                <li>
                    <a href="#pageSubmenu4" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-users"></i>
                        Reports
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu4">
                        <li>
                            <a href="reports/sales.php">Sales</a>
                        </li>
                        <li>
                            <a href="">Purchase</a>
                        </li>
                        <li>
                            <a href="">Stock</a>
                        </li>
                    </ul>
                </li>

                
                <li>
                    <a href="#pageSubmenu3" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-users"></i>
                        User
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu3">
                        <li>
                            <a href="Login.php">Login</a>
                        </li>
                        <li>
                            <a href="register.html">Register</a>
                        </li>
                        
                    </ul>
                </li>
                
               <!--  <li>
                    <a href="maps.html">
                        <i class="far fa-map"></i>
                        Maps
                    </a>
                </li> -->
            </ul>
        </nav>