<div class="container">
            <!-- main-heading -->
            <h2 class="main-title-w3layouts mb-2 text-center text-white"><font color="black">Insert Meal</font></h2>
            <!--// main-heading -->
            <div class="form-body-w3-agile text-center w-lg-50 w-sm-75 w-100 mx-auto mt-5">
                <form action="#" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Meal Name</label>
                        <input type="text" name="txt1" class="form-control" placeholder="Enter meal name
                        " required="">
                    </div>
                    <div class="form-group">
                        <label>Meal Description</label>
                        <input type="text" name="txt2" class="form-control" placeholder="Enter Description" required="">
                    </div>
                    <div class="form-group">
                        <label>Meal Price (in Rs.)</label>
                        <input type="number" name="txt3" class="form-control" placeholder="Price" required="">
                    </div>
                    <div class="form-group">
                        <label>Meal Image</label>
                        <input type="file" name="txt4" class="form-control" placeholder="Password" required="">
                    </div>
                    <div class="form-group">
                        <label>Cuisine Type</label>
                        <select name="s1" class="form-control">
                        <?php 

                            require './Connect.php';

                            $catq = mysqli_query($Connect,"SELECT
                                                             * 
                                                            FROM 
                                                            cuisine") 
                                                            or die
                                                            (mysqli_error($connect));
                            while ($catrow = mysqli_fetch_array($catq))
                            {    

                                echo "<option value='{$catrow['cuisine_id']}'>{$catrow['cuisine_name']}</option>";
                                
                              
                            }
                        ?>    
                        </select>
                        
                    </div>    
                   
                    <button type="submit" name="submit" class="btn btn-primary error-w3l-btn mt-sm-5 mt-3 px-4">Insert</button>
                    <button type="submit" onclick="window.location='Mealshow.php'" name="mealins" class="btn btn-primary error-w3l-btn mt-sm-5 mt-3 px-4">Show</button>
                </form>
                <!-- <p class="paragraph-agileits-w3layouts mt-4">Already have account?
                    <a href="login.html">Login</a>
                </p> -->
                <!-- <h1 class="paragraph-agileits-w3layouts mt-2">
                    <a href="index.html">Back to Home</a>
                </h1> -->
            </div>

<?php

if ($_POST) 
{
   require './Connect.php';

    $mealname = $_POST['txt1'];
    $mealdesc = $_POST['txt2'];
    $mealprice = $_POST['txt3'];
    $mealimage = $_FILES['txt4']['name'];
    $cuisineid = $_POST['s1'];


    $mealinsert = mysqli_query($Connect,"INSERT INTO 
                                                meal
                                                (meal_name, 
                                                meal_description,
                                                meal_price,
                                                meal_image,
                                                cuisine_id) 
                                            VALUES 
                                                ('{$mealname}',
                                                 '{$mealdesc}',
                                                 '{$mealprice}',
                                                 '{$mealimage}',
                                                 '{$cuisineid}')
                                                 ") 
                                    or die(mysqli_error($connect));

    if ($mealinsert) 
    {
        $imageq = move_uploaded_file($_FILES['txt4']['tmp_name'], "Upload/".$mealimage);
        echo "record inserted succesfully";
    }
    else
    {
        echo "failed";
    }



}


?>