<?php
require './Connect.php';

session_start();


if ($_POST) 
{
    $uname = $_POST['uname'];
    $paswd = $_POST['upass'];

    $q = mysqli_query($Connect,"SELECT 
                                    *  
                                FROM 
                                    `admin` 
                                WHERE 
                                    admin_email='{$uname}' 
                                AND 
                                    admin_password='{$paswd}' 
                                ")
                                or die(mysqli_error($Connect));

    while ($row = mysqli_fetch_array($q)) 
    {
        $_SESSION['uname'] = $row['admin_email'];

        echo $_SESSION['uname'];
        header("location:index.php");
    }
    
}


?>


<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">

<?php

include './themepart/Head.php';

?>

<body>
    <div class="bg-page py-5">
        <div class="container">
            <!-- main-heading -->
            <h2 class="main-title-w3layouts mb-2 text-center text-black">Login to Aahar</h2>
            <!--// main-heading -->
            <div class="form-body-w3-agile text-center w-lg-50 w-sm-75 w-100 mx-auto mt-5">
                <form action="#" method="post">
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" name="uname" class="form-control" placeholder="Enter email" required="">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="upass" class="form-control" placeholder="Password" required="">
                    </div>
                    
                    <button type="submit" name="submit" class="btn btn-primary error-w3l-btn mt-sm-5 mt-3 px-4">Login</button>
                </form>
                
            </div>

            <!-- Copyright -->
            <?php


            include './themepart/footer.php';

            ?>
            <!--// Copyright -->
        </div>
    </div>


    <!-- Required common Js -->
    <script src='js/jquery-2.2.3.min.js'></script>
    <!-- //Required common Js -->

    <!-- Js for bootstrap working-->
    <script src="js/bootstrap.min.js"></script>
    <!-- //Js for bootstrap working -->

</body>

</html>