<div class="container">
            <!-- main-heading -->
            <h2 class="main-title-w3layouts mb-2 text-center text-white"><font color="black">Insert Employee</font></h2>
            <!--// main-heading -->
            <div class="form-body-w3-agile text-center w-lg-50 w-sm-75 w-100 mx-auto mt-5">
<form action="#" method="post" enctype="multipart/form-data">
    <table>

                    <div class="form-group">
                        <tr>
                        <td><label>First Name</label></td>
                        <td><input type="text" name="empfrstname" class="form-control" placeholder="Enter Firstname" required=""></td>
                        </tr>
                    </div>
                     <div class="form-group">
                        <tr>
                        <td><label>Last Name</label></td>
                        <td><input type="text" name="emplstname" class="form-control" placeholder="Enter Lastname" required=""></td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                        <td><label>Gender</label></td>
                        <td>
                            <input type="radio" name="empgenrb" value="Male" class="form-control"  required="">Male 
                            <input type="radio" name="empgenrb" value="Female" class="form-control" required="">Female
                        </td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                        <td><label>Email address</label></td>
                        <td><input type="email" name="empmail" class="form-control" placeholder="Enter email" required=""></td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                        <td><label>Number</label></td>
                        <td><input type="number" name="empnumber" class="form-control" placeholder="Enter username" required=""></td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                        <td><label>Addressline 1</label></td>
                        <td><input type="text" name="empadd1" class="form-control" placeholder="Enter Address" required=""></td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                        <td><label>Addressline 2</label></td>
                        <td><input type="text" name="empadd2" class="form-control" placeholder="Enter Address" required=""></td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                        <td><label>QRCode</label></td>
                        <td><input type="file" name="empqrcode" class="form-control" required="" align="center"></td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                        <td><label>Password</label></td>
                        <td><input type="password" name="empaswd" class="form-control" placeholder="Enter Password" required=""></td>
                        </tr>
                    </div>
                    
                    <tr>
                    <!-- <div class="form-check text-center">
                        <td><input type="checkbox" class="form-check-input" id="exampleCheck1"></td>
                        <td><label class="form-check-label" for="exampleCheck1">Agree the terms and policy</label></td>
                    </div> -->
                    </tr>
                    <tr><td><button type="submit" name="empins" class="btn btn-primary error-w3l-btn mt-sm-5 mt-3 px-4">Insert</button></td>

                    <td><button type="submit" onclick="window.location='Employeeshow.php'" name="empins" class="btn btn-primary error-w3l-btn mt-sm-5 mt-3 px-4">Show</button></td></tr>
                    </table>
                </form>

<?php
require './Connect.php';
if (isset($_POST['empins'])) 
{
    $empfname = $_POST['empfrstname'];
    $emplname = $_POST['emplstname'];
    $empgen = $_POST['empgenrb'];
    $empmail = $_POST['empmail'];
    $empnum = $_POST['empnumber'];
    $empadd1 = $_POST['empadd1'];
    $empadd2 = $_POST['empadd2'];
    $empqrc = $_FILES['empqrcode']['name'];
    $emppaswd = $_POST['empaswd'];

    //echo $empgen;   

    $empq = mysqli_query($Connect,"INSERT INTO 
                                        `employee`
                                    (`employee_firstname`, 
                                     `employee_lastname`,
                                     `employee_gender`, 
                                     `employee_email`, 
                                     `employee_contactnumber`, 
                                     `employee_adressline1`, 
                                     `employee_addressline2`, 
                                     `employee_QRCode`, 
                                     `employee_password`) 
                                  VALUES 
                                    ('{$empfname}',
                                     '{$emplname}',
                                     '{$empgen}',
                                     '{$empmail}',
                                     '{$empnum}',
                                     '{$empadd1}',
                                     '{$empadd2}',
                                     '{$empqrc}',
                                     '{$emppaswd}')

                                     ")
                                or die(mysqli_error($Connect));
    if ($empq) 
    {
        $fileupload = move_uploaded_file($_FILES['empqrcode']['tmp_name'],  "Upload/".$empqrc);
        if ($fileupload) 
        {
            echo "Inserted :";
        }
    }


}


?>